const mongoose = require("mongoose");

const ChatSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    min: 3,
    max: 20,
  },
  members: {
    type: Array
  }
});

module.exports = mongoose.model("Chats", ChatSchema);
