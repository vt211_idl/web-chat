const User = require("../models/userModel");
const bcrypt = require("bcrypt");
const { generateAccessToken } = require("../services/auth");
const jwt = require("jsonwebtoken");

module.exports.login = async (req, res, next) => {
  try {
    const { userName, password } = req.body;
    const user = await User.findOne({ userName });
    if (!user)
      return res.json({ msg: "Incorrect Username or Password", status: false });
    const isPasswordValid = await bcrypt.compare(password, user.password);

    const token = generateAccessToken({
      userName: user.userName,
      _id: user._id,
    });
    if (!isPasswordValid)
      return res.json({ msg: "Incorrect Username or Password", status: false });
    delete user.password;
    return res.status(200).json({ status: true, user, token });
  } catch (ex) {
    next(ex);
  }
};

module.exports.register = async (req, res, next) => {
  try {
    const { userName, email, password, confirmPassword } = req.body;
    const usernameCheck = await User.findOne({ userName });

    if (usernameCheck)
      return res.json({ msg: "Username already used", status: false });
    if (password !== confirmPassword) {
      return res.json({ msg: "Password does not match", status: false });
    }
    const emailCheck = await User.findOne({ email });
    
    if (emailCheck)
      return res.json({ msg: "Email already used", status: false });

    const hashedPassword = await bcrypt.hash(password, 10);
    const avatarColor = Math.floor(Math.random() * 16777215).toString(16);

    const user = await User.create({
      email,
      userName,
      avatarColor,
      password: hashedPassword,
    });
    delete user.password;
    return res.status(200).json({ status: true });
  } catch (ex) {
    next(ex);
  }
};

module.exports.getAllUsers = async (req, res, next) => {
  try {
    const token = req.headers["authorization"];
    const { _id } = jwt.decode(token);
    
    const users = await User.find({_id: { $ne: _id }}).select([
      "userName",
      "avatarColor",
      "_id",
    ]);
    return res.json(users);
  } catch (ex) {
    next(ex);
  }
};

module.exports.logOut = (req, res, next) => {
  try {
    if (!req.params.id) return res.json({ msg: "User id is required " });
    onlineUsers.delete(req.params.id);
    return res.status(200).send();
  } catch (ex) {
    next(ex);
  }
};
