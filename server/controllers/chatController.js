const Chat = require("../models/chatModel");

module.exports.create = async (req, res, next) => {
  try {
    const { name } = req.body;

    const chat = await Chat.create({
      name,
    });
    return res.json({ status: true, chat });
  } catch (ex) {
    next(ex);
  }
};

module.exports.rename = async (req, res, next) => {
  try {
    const { id, name } = req.body;

    const chat = await Chat.findOne({
      _id: id,
    });

    if (!chat) return res.json({ msg: "Chat does not exists", status: false });

    await Chat.findOneAndUpdate({ _id }, { ...chat, name });

    return res.json({ status: true, chat });
  } catch (ex) {
    next(ex);
  }
};

module.exports.addMember = async (req, res, next) => {
  try {
    const { memberId, _id } = req.body;

    const chat = await Chat.findOne({ _id });

    if (!chat) return res.json({ msg: "Chat does not exists", status: false });

    await Chat.findOneAndUpdate(
      { _id },
      { ...chat, members: [...chat.members, memberId] }
    );

    return res.json({ status: true, chat });
  } catch (ex) {
    next(ex);
  }
};

// module.exports.removeMember = async (req, res, next) => {
//   try {
//     const { memberId, _id } = req.body;

//     const chat = await Chat.findOne({ _id });

//     if (!chat) return res.json({ msg: "Chat does not exists", status: false });

//     await Chat.findOneAndUpdate({ _id }, {...chat, members: [...chat.members, memberId]});

//     return res.json({ status: true, chat });
//   } catch (ex) {
//     next(ex);
//   }
// };

module.exports.getAllChats = async (res, next) => {
  try {
    const { id } = req.params;
    const chats = await Chat.find({ members: { $all: [id] } });
    if (!chats) return res.json({ msg: "You don't have chats", status: false });
    return res.json(chats);
  } catch (ex) {
    next(ex);
  }
};
