const Message = require("../models/messageModel");
const jwt = require("jsonwebtoken");

module.exports.getMessages = async (req, res, next) => {
  try {
    const { id } = req.params;
    const token = req.headers["authorization"];
    const { _id } = jwt.decode(token);

    const messages = await Message.find({
      $or: [{ users: [_id, id] }, { users: [id, _id] }],
    }).sort({ updatedAt: 1 });

    const projectedMessages = messages.map((msg) => {
      return {
        fromSelf: msg.sender.toString() === _id,
        message: msg.message.text,
        time: msg.updatedAt,
        id: msg._id,
      };
    });
    res.json(projectedMessages);
  } catch (ex) {
    next(ex);
  }
};

module.exports.addMessage = async (req, res, next) => {
  try {
    const { chatId, message } = req.body;
    const token = req.headers["authorization"];
    const { _id } = jwt.decode(token);

    const data = await Message.create({
      message: { text: message },
      users: [_id, chatId],
      sender: _id,
    });
    if (data)
      return res.json({
        fromSelf: true,
        message: data.message.text,
        time: data.updatedAt,
        id: data._id,
        sender: data.sender,
        adress: chatId
      });
    else return res.json({ msg: "Failed to add message to the database" });
  } catch (ex) {
    next(ex);
  }
};
