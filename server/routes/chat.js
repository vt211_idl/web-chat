const { create, getAllChats, getChats } = require("../controllers/chatController");
const router = require("express").Router();
const { authenticateToken } = require("../middlewares/guard");

router.post("/", create);
router.get("/:id", getAllChats);
router.post("/rename", getAllChats);

module.exports = router;
