const { addMessage, getMessages } = require("../controllers/messageController");
const router = require("express").Router();
const { authenticateToken } = require("../middlewares/guard");

router.post("/", authenticateToken, addMessage);
router.get("/:id", authenticateToken, getMessages);

module.exports = router;
