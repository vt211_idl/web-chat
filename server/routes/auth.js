const {
  login,
  register,
  getAllUsers,
  logOut,
} = require("../controllers/userController");
const { authenticateToken } = require("../middlewares/guard");
const router = require("express").Router();

router.post("/login", login);
router.post("/register", register);
router.get("/allusers/", authenticateToken, getAllUsers);
router.get("/logout/:id", logOut);

module.exports = router;
