import { ActionReducerMapBuilder } from "@reduxjs/toolkit";
import {
  getChatAsync,
  getChatListAsync,
  sendMessageAsync,
  setActiveChat,
  setMessageFromIO,
} from "./actions";
import { ChatState, Chat } from "./types";

export const getChatMessagesReducer = (
  builder: ActionReducerMapBuilder<ChatState>
) => {
  builder.addCase(getChatAsync.fulfilled, (state, action) => {
    state.isLoadingMessages = false;
    state.selectedChat = action.payload;
  });

  builder.addCase(getChatAsync.pending, (state) => {
    state.isLoadingMessages = true;
    state.selectedChat = undefined;
  });

  builder.addCase(getChatAsync.rejected, (state) => {
    state.isLoadingMessages = false;
    state.selectedChat = null;
  });
};

export const getChatListReducer = (
  builder: ActionReducerMapBuilder<ChatState>
) => {
  builder.addCase(getChatListAsync.fulfilled, (state, action) => {
    state.isLoadingList = false;
    state.chatList = action.payload!.map((chat: Chat) => ({
      ...chat,
      newMessages: false,
    }));
    console.log(action.payload);
  });

  builder.addCase(getChatListAsync.pending, (state) => {
    state.isLoadingList = true;
    state.chatList = null;
  });

  builder.addCase(getChatListAsync.rejected, (state) => {
    state.isLoadingList = false;
    state.chatList = null;
  });
};

export const setActiveChatReducer = (
  builder: ActionReducerMapBuilder<ChatState>
) => {
  builder.addCase(setActiveChat, (state, action) => {
    state.activeChat = action.payload;
    state.chatList = state.chatList!.map((chat: Chat) =>
      chat._id === action.payload._id
        ? {
            ...chat,
            newMessages: false,
          }
        : chat
    );
  });
};

export const sendMessageAsyncReducer = (
  builder: ActionReducerMapBuilder<ChatState>
) => {
  builder.addCase(sendMessageAsync.fulfilled, (state, action) => {
    state.currentSendingMessage = action.payload;
    state.selectedChat.push(action.payload);
  });
};

export const setMessageFromIOReducer = (
  builder: ActionReducerMapBuilder<ChatState>
) => {
  builder.addCase(setMessageFromIO, (state, action) => {
    const message = action.payload.msg;
    const chatList = action.payload.chatList;

    const candidate = chatList.find((chat) => chat._id === message.sender);

    if (candidate?._id === state.activeChat?._id) {
      const { adress, ...item } = message;

      state.selectedChat.push({ ...item, fromSelf: adress === candidate?._id });
    } else {
      console.log("not that chat");

      state.chatList = chatList.map((chat: Chat) =>
        chat._id === message.sender
          ? {
              ...chat,
              newMessages: true,
            }
          : chat
      );
    }
  });
};
