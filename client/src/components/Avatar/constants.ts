export const AVATAR_COLORS: string[] = [
  'bg-[#827397]',
  'bg-[#FF7B54]',
  'bg-[#FFB26B]',
  'bg-[#B2861B]',
  'bg-[#645CAA]',
  'bg-[#85586F]',
  'bg-[#8E3200]',
  'bg-[#363062]',
];

export enum AVATAR_SIZES {
  XXS = 'w-9 h-9 text-base',
  XS = 'w-12 h-12 text-2xl',
  S = 'w-16 h-16 text-4xl',
  M = 'w-20 h-20 text-5xl',
  L = 'w-32 h-32 text-6xl',
  XL = 'w-36 h-36 text-7xl',
}
