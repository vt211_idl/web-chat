import React from 'react';

const Default = () => {
  return (
    <div className='w-full h-[800px] flex flex-col justify-center items-center bg-dark relative select-none'>
      <span className='px-4 bg-gray-0 rounded text-2xl'>Select chat</span>
    </div>
  );
};

export default Default;
